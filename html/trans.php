<div class="submenu">
<?php
$trans = array(
        'en' => 'English',
        'fr' => 'français',
);

$array = explode("/", $page);
if (count($array) == 1) {
	# index page: index.php.<lang>
	$array = explode(".", $page);
	$lang = array_pop($array);
	$loc = implode(".", $array);
} else {
	# manual page: build/<lang>/man<section>/<page>.<section>
	$lang = $array[1];
	$loc = $array[3].".php";
}
$first = 1;

if ($lang != "C" && $lang != "en") {
  echo "<a href='$loc.en'>".$trans['en']."</a>";
  $first = 0;
} else
  $lang = 'en';

foreach (glob("$loc.*") as $filename) {
        $other = explode(".", $filename);
        $other_lang = array_pop($other);
        if (($other_lang != $lang) && ($other_lang != "en")) {
                if (!$first)
                        echo "/ ";
                echo "<a href='$loc.$other_lang'>".$trans[$other_lang]."</a>";
                $first = 0;
        }
}
function upstream_page($page, $lang)
{
	if ($lang != 'en')
		return;
	# Replace build/C/manX/foo.X by pages/manX/foo.X
	$page = substr_replace($page, "pages", 0, 7);
	echo <<<END
<div class="content">
<p>English pages are provided only to compare translations to original pages,
better looking pages can be <a href="http://man7.org/linux/man-pages/online/$page.html">browsed</a>
at the <a href="http://man7.org/linux/man-pages/">Linux man-pages</a> official site.</p>
</div><div class="spacer"></div>
END;
}
?>
</div>
<div class="spacer"></div>
