PO4AFLAGS = -k 100
VERSION = 3.70-1

LANGS = fr

html: setuphtml html-en $(patsubst %, html-%, $(LANGS))
setuphtml:
	-@rm -rf build
	@cp -a ../man-pages-fr/build .
	@cp -a html build/
	@for i in $$(seq 8); do mkdir -p build/html/online/man$$i; done
	@po4a $(PO4AFLAGS) --rm-backups html.cfg

html-%:
	@set -e; for f in build/html/*.$* build/html/online/index.php.$*; do \
	  test -e $$f || continue; \
	  sed -i -e 's/\.en";/.$*";/g' -e "s/\\.en';/.$*';/g" $$f; \
	done
	@set -e; for man in build/C/man*/*; do \
	  secman=$${man#build/C/}; \
	  page=build/$*/$$secman; \
	  test ! $* = en || page=build/C/$$secman; \
	  test -e $$page || continue; \
	  name=$$(echo $$secman | sed -e 's,^man./\(.*\)\.\([0-9]\)$$,\1(\2),'); \
	  man2html -r $$page |\
	    sed -e '/Content-type: text.html/d' \
	        -e 's,&lt;URL:\(<A HREF="[^"]*">[^<>]*</A>\)&gt;,\1,' \
	        -e 's,&lt;<A HREF="file://[^"]*">\([^<>]*\)</A>&gt;,\&lt;\1\&gt;,' \
	        -e '/^Section:/,/<HR>/{s/Section: //; s/Updated: //; s/<BR><A .*/<HR>/; s/<A .*//; }' \
	        -e 's,\(<A HREF="[^"]*\).html">,\1.php">,' \
	        -e 's,<HEAD>,<head><link rel="stylesheet" title="Default Style" type="text/css" href="../../styles.css"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />,' \
	        -e 's|<BODY>|<body><? $$section = "manual"; $$topdir = "../../"; include "$${topdir}header.php.$*"; ?><div id="body"><?php $$page="'$$page'"; include "$${topdir}trans.php"; ?><? upstream_page("$$page", "$*"); ?><div class="content">|' \
	        -e 's,</BODY>,</div><? include "$${topdir}footer.php.$*"; ?></div></body>,' \
	      > build/html/online/$$secman.php.$*; \
	  title=$$(lexgrog "$$page" | sed -e '1!d' -e 's/.*: \".* - //' -e 's/"$$//'); \
	  printf "<tr><td><a href=\"%s\">%s</a></td><td>%s</td></tr>\n" $$secman.php "$$name" "$$title" >> build/html/online/index.php.$*; \
	done
	@printf "</table></p>\n</div>\n<? include \"\$${topdir}footer.php.$*\"; ?>\n</div>\n</body>\n</html>\n" >> build/html/online/index.php.$*

update:
	for f in po/fr.po po/perkamon-web.pot; do msgcat -w 0 -o $$f $$f; done
	sed -i -e "s/perkamon v[0-9.]*-[0-9]*/perkamon v$(VERSION)/" html/download.php.en html/online/index.php.en po/fr.po po/perkamon-web.pot
	for f in po/fr.po po/perkamon-web.pot; do msgcat -o $$f $$f; done

upload:
	scp -r build/html/online/* alioth.debian.org:/srv/home/groups/perkamon/htdocs/online/
	scp build/html/*.* alioth.debian.org:/srv/home/groups/perkamon/htdocs/
	-ssh alioth.debian.org chgrp -R perkamon /srv/home/groups/perkamon/htdocs
	-ssh alioth.debian.org chmod -R g+rw /srv/home/groups/perkamon/htdocs

.PHONY: setuphtml
